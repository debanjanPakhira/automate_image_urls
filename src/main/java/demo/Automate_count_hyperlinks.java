package demo;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
//Selenium Imports
import org.openqa.selenium.Keys;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
///


public class Automate_count_hyperlinks {
    ChromeDriver driver;
    public Automate_count_hyperlinks()
    {
        System.out.println("Constructor: TestCases");
        WebDriverManager.chromedriver().timeout(30).setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }

    public void endTest()
    {
        System.out.println("End Test: TestCases");
        driver.close();
        driver.quit();

    }


    public  void testCase01(){
        System.out.println("Start Test case: testCase01");
        driver.get("https://in.bookmyshow.com/explore/home/chennai");

        WebElement RecomendedMovies = driver.findElement(By.xpath("//*[@id=\"super-container\"]/div[2]/div[3]/div[1]/div[1]/div/div/div/div[2]/div"));


        List<WebElement> allImage = RecomendedMovies.findElements(By.tagName("img"));
        System.out.println("the number of Image Link: "+allImage.size());

        for(WebElement image:allImage){
            System.out.println(image.getText() + " - " + image.getAttribute("src"));
        }
        System.out.println("end Test case: testCase02");
    }


}
